# coding: utf-8
import subprocess
import sys

# 폴더 리스트를 가져오는 함수
def load_stulist(dir):
    stu_string=subprocess.check_output('ls '+dir, shell=True)
    return stu_string.decode("utf8").split('\n')

def compile_c(list, num, dir):
    for l in list:
        pwd=dir+'/'+l+'/c'+num+'_'+l
        # 경로+문제번호+학번 ex> input/20180000/c1_20180000
        subprocess.call('clang '+pwd+'.c -o '+pwd, shell=True)

    return 

def run_problem(num, l, dir):
    
    fcmd='./'+dir+'/'+l+'/c'+num+'_'+l
    print(l)
    if num=='1':
        number_one(fcmd)
    if num=='2':
        number_two(fcmd)
    if num=='3':
        number_three(fcmd)
    if num=='4':
        number_four(fcmd)

#문제 테스트케이스 돌리기
def number_one(fcmd):
    #문제 1번 예제 1
    input_yone=b'30 70 120 130 50'
    output_yone=b'80.90 bad'
    print(check_problem(fcmd, input_yone, output_yone))
    #문제 1번 test 1
    input_tone=b'125 20 10 0 80'
    output_tone=b'47.70 normal'
    print(check_problem(fcmd, input_tone, output_tone))
    #문제 1번 test 2
    input_ttwo=b'0 0 0 0 0'
    output_ttwo=b'0.00 good'
    print(check_problem(fcmd, input_ttwo, output_ttwo))
    #문제 1번 test 3
    input_tthree=b'200 200 200 200 200'
    output_tthree=b'200.00 very bad'
    print(check_problem(fcmd, input_tthree, output_tthree))
    #문제 1번 test 4S
    input_tfour=b'1 1 1 1 1'
    output_tfour=b'1.00 good'
    print(check_problem(fcmd, input_tfour, output_tfour))
    #문제 1번 test 5
    input_tfive=b'0 29 79 149 1000'
    output_tfive=b'550.84 very bad'
    print(check_problem(fcmd, input_tfive, output_tfive))

def number_two(fcmd):
    #문제 2번 예제 1
    input_yone=b'd2'
    output_yone=b'B'
    print(check_problem(fcmd, input_yone, output_yone))
    #문제 2번 test 1
    input_tone=b'a2'
    output_tone=b'W'
    print(check_problem(fcmd, input_tone, output_tone))
    #문제 1번 test 2
    input_ttwo=b'g6'
    output_ttwo=b'W'
    print(check_problem(fcmd, input_ttwo, output_ttwo))
    #문제 1번 test 3
    input_tthree=b'h8'
    output_tthree=b'B'
    print(check_problem(fcmd, input_tthree, output_tthree))
    #문제 1번 test 4S
    input_tfour=b'a1'
    output_tfour=b'B'
    print(check_problem(fcmd, input_tfour, output_tfour))

def number_three(fcmd):
    #문제 3번 예제 1
    input_yone=b'3 cat'
    output_yone=b'fdw'
    print(check_problem(fcmd, input_yone, output_yone))
    #문제 2번 test 1
    input_tone=b'3 xyz'
    output_tone=b'abc'
    print(check_problem(fcmd, input_tone, output_tone))
    #문제 1번 test 2
    input_ttwo=b'4 luv'
    output_ttwo=b'pyz'
    print(check_problem(fcmd, input_ttwo, output_ttwo))
    #문제 1번 test 3
    input_tthree=b'-1 bad'
    output_tthree=b'azc'
    print(check_problem(fcmd, input_tthree, output_tthree))
    #문제 1번 test 4S
    input_tfour=b'2 aaa'
    output_tfour=b'ccc'
    print(check_problem(fcmd, input_tfour, output_tfour))
    #문제 1번 test 4S
    input_tfive=b'-1 aaa'
    output_tfive=b'zzz'
    print(check_problem(fcmd, input_tfive, output_tfive))

def number_four(fcmd):
    #문제 3번 예제 1
    input_yone=b'-4.0 -2.0 2.0 5.0'
    output_yone=b'5.0'
    print(check_problem(fcmd, input_yone, output_yone))
    #문제 2번 test 1
    input_tone=b'-4.3 1.7 3.1 5.0'
    output_tone=b'3.3'
    print(check_problem(fcmd, input_tone, output_tone))
    #문제 1번 test 2
    input_ttwo=b'-5.0 -4.3 -4.0 -3.9'
    output_ttwo=b'0.4'
    print(check_problem(fcmd, input_ttwo, output_ttwo))
    #문제 1번 test 3
    input_tthree=b'0.2 0.3 0.4 0.5'
    output_tthree=b'0.2'
    print(check_problem(fcmd, input_tthree, output_tthree))
    #문제 1번 test 4S
    input_tfour=b'-4.0 0.0 2.0 3.0'
    output_tfour=b'3.0'
    print(check_problem(fcmd, input_tfour, output_tfour))
    #문제 1번 test 4S
    input_tfive=b'-5.0 0.0 1.0 5.0'
    output_tfive=b'5.0'
    print(check_problem(fcmd, input_tfive, output_tfive))



def check_problem(fcmd, inputstd, outputstd):
    p = subprocess.Popen(fcmd, shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    result=p.communicate(input=inputstd)[0]
    #print("student output: "+result.decode())
    #print("answer: "+outputstd.decode())
    boutput = (outputstd.decode()+'\n').encode()
    if result==outputstd or result==boutput:
        return 1
    else:
        return 0

def run_program(dir, stage):
    stulist = load_stulist(dir)

    #stage 0:답안지 컴파일 과정
    if stage == 0:
        try:
            for i in range(1,5):
                compile_c(stulist, str(i), dir)

        except:
            print("an error occured while compile answersheet")
    
    if stage == 1:
    #stage 1: 문제 답안 맞추는 과정
        try:
            for stu in stulist:
                run_problem('1', stu, dir)

        except:
            print("An error occured while checking problem 1 ")

    if stage == 2:
    #stage 2: 문제 답안 맞추는 과정
        try:
            for stu in stulist:
                run_problem('2', stu, dir)

        except:
            print("An error occured while checking problem 2")

    if stage == 3:
    #stage 3: 문제 답안 맞추는 과정
        try:
            for stu in stulist:
                run_problem('3', stu, dir)

        except:
            print("An error occured while checking problem 3")

    if stage == 4:
    #stage 4: 문제 답안 맞추는 과정
        try:
            for stu in stulist:
                run_problem('4', stu, dir)

        except:
            print("An error occured while checking problem 3")



if __name__ == "__main__":
    dir=sys.argv[1]
#stulist = load_stulist(dir)
    #compile_c(stulist,'1',dir)
    #run_problem('1', '20181629', dir)
    run_program(dir, int(sys.argv[2]))

